import React from 'react';
import ReactDOM from 'react-dom';

import {UIRouter, UIView, pushStateLocationPlugin} from '@uirouter/react';
import { states, config } from './routes';

import './index.scss';

const fragment = 
  <UIRouter plugins={[pushStateLocationPlugin]}
            states={states}
            config={config}
  >
    <UIView />
  </UIRouter>;

ReactDOM.render(fragment, document.getElementById('app'));