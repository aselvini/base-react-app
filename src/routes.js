import Layout from "./components/layout/index.jsx";
import Home from "./components/home/index.jsx";


const layoutDefault = {
  name: 'layoutDefault',
  abstract: 'true',
  component: Layout
};

const home = {
  parent: 'layoutDefault',
  name: 'home',
  url: '/',
  component: Home
};

const states = [
  layoutDefault,
  home
];

// Set initial state of the router and stuff
const config = router => {
  router.urlService.rules.initial({ state: "home" });
};

export {
  states,
  config
};